#-*- coding: utf-8 -*-
from main.models import Menu, Event, Document, MenuItem, Post
from datetime import datetime

def menu(request):
    try:
        parentmenu = Menu.objects.get(title="Main menu")
    except:
        pass
    else:
        menu = MenuItem.objects.filter(menu=parentmenu, is_active=True).order_by('position')

    cp_event_block = Event.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:3]
    cp_document_block = Document.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:3]
    cp_news_block = Post.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:3]
    return locals()

