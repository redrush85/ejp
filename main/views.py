from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.shortcuts import render_to_response
from django.template import RequestContext

from datetime import datetime

from main.models import Post, Event, Document, Member, Feedback, Country


articles_on_page = 8

class PostDetailView(DetailView):
    model = Post


class PostListView(ListView):
    model = Post
    paginate_by = articles_on_page

    def get_queryset(self):
        return Post.objects.filter(date__lte=datetime.now(), is_active=True).order_by('-date')


class EventDetailView(DetailView):
    model = Event


class EventListView(ListView):
    model = Event
    paginate_by = articles_on_page

    def get_queryset(self):
        return Event.objects.filter(date__lte=datetime.now(), is_active=True).order_by('-date')


class DocumentDetailView(DetailView):
    model = Document


class DocumentListView(ListView):
    model = Document
    paginate_by = articles_on_page

    def get_queryset(self):
        return Document.objects.filter(date__lte=datetime.now(), is_active=True).order_by('-date')


class MemberDetailView(DetailView):
    model = Member


class MemberListView(ListView):
    model = Member

    def get_context_data(self, **kwargs):
        context = super(MemberListView, self).get_context_data(**kwargs)
        context['countries'] = Country.objects.all().order_by('name')

        return context


class FeedbackCreateView(CreateView):
    model = Feedback
    fields = ['name', 'email', 'message']
    success_url = '/thanks/'


class ThanksView(TemplateView):
    template_name = "thanks.html"


def home(request):
    news_block = Post.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:5]
    events_block = Event.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:4]
    documents_block = Document.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:5]
    member_block = Member.objects.all().exclude(image='').order_by('?')[:12]

    return render_to_response("index.html", {"news_block": news_block, 'member_block': member_block, 'events_block': events_block, 'documents_block': documents_block}, context_instance=RequestContext(request))
