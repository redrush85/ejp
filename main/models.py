# -*- coding: utf-8 -*-

from django.db import models
from sorl.thumbnail import ImageField
from sorl.thumbnail import get_thumbnail
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from django.conf import settings
from django.core.urlresolvers import reverse


# Create your models here.
class Menu(models.Model):
    title = models.CharField(max_length=100)

    def __unicode__(self):
        return self.title

    class Meta:

        verbose_name = "Menu"
        verbose_name_plural = "Menu"


class MenuItem(models.Model):
    menu = models.ForeignKey(Menu)
    title = models.CharField(max_length=100)
    url = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True, verbose_name='Is active')
    position = models.PositiveSmallIntegerField(default=0, verbose_name='Sort position')

    class Meta:
        ordering = ('position',)
        verbose_name = "Menu Item"
        verbose_name_plural = "Menu Items"


class Post(models.Model):
    title = models.CharField(max_length=255)
    content = RichTextField()
    image = ImageField(upload_to='uploads/news/')
    date = models.DateTimeField()
    is_active = models.BooleanField(default=True, verbose_name="Published")
    slug = models.SlugField(unique=True, verbose_name="URL", max_length=255)

    def __unicode__(self):
        return self.title

    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '100x100', crop='center')
            return '<img src="%s">' % im.url
        return ''

    preview.allow_tags = True

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"

    def get_absolute_url(self):
        return reverse("post-detail", kwargs={'slug': self.slug})



class Event(models.Model):
    title = models.CharField(max_length=255)
    content = RichTextField()
    image = ImageField(upload_to='uploads/events/')
    date = models.DateTimeField()
    is_active = models.BooleanField(default=True, verbose_name="Published")
    slug = models.SlugField(unique=True, verbose_name="URL", max_length=255)

    def __unicode__(self):
        return self.title

    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '100x100', crop='center')
            return '<img src="%s">' % im.url
        return ''

    preview.allow_tags = True

    class Meta:
        verbose_name = "Event"
        verbose_name_plural = "Events"

    def get_absolute_url(self):
        return reverse("event-detail", kwargs={'slug': self.slug})


class Document(models.Model):
    title = models.CharField(max_length=255)
    content = RichTextField()
    date = models.DateTimeField()
    is_active = models.BooleanField(default=True, verbose_name="Published")
    slug = models.SlugField(unique=True, verbose_name="URL", max_length=255)

    def __unicode__(self):
        return self.title

    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '100x100', crop='center')
            return '<img src="%s">' % im.url
        return ''

    preview.allow_tags = True

    class Meta:
        verbose_name = "Document"
        verbose_name_plural = "Documents"

    def get_absolute_url(self):
        return reverse("document-detail", kwargs={'slug': self.slug})


class Country(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Country"
        verbose_name_plural = "Countries"
        ordering = ['name']


class Member(models.Model):
    name = models.CharField(max_length=255)
    country = models.ForeignKey(Country)
    image = ImageField(upload_to='uploads/members', blank=True)
    email = models.EmailField()
    cv = RichTextField(blank=True)

    def __unicode__(self):
        return self.name

    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '100x100', crop='center')
            return '<img src="%s">' % im.url
        return ''

    preview.allow_tags = True

    class Meta:
        verbose_name = "Member"
        verbose_name_plural = "Members"

    def get_absolute_url(self):
        return reverse("member-detail", kwargs={'pk': self.pk})


class Feedback(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    message = models.TextField()

