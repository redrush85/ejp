# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('content', ckeditor.fields.RichTextField()),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=b'uploads/documents/')),
                ('date', models.DateTimeField()),
                ('is_active', models.BooleanField(default=True, verbose_name=b'Published')),
                ('slug', models.SlugField(unique=True, verbose_name=b'URL')),
            ],
            options={
                'verbose_name': 'Document',
                'verbose_name_plural': 'Documents',
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('content', ckeditor.fields.RichTextField()),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=b'uploads/events/')),
                ('date', models.DateTimeField()),
                ('is_active', models.BooleanField(default=True, verbose_name=b'Published')),
                ('slug', models.SlugField(unique=True, verbose_name=b'URL')),
            ],
            options={
                'verbose_name': 'Event',
                'verbose_name_plural': 'Events',
            },
        ),
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=b'uploads/members', blank=True)),
                ('email', models.EmailField(max_length=254)),
                ('cv', ckeditor.fields.RichTextField(blank=True)),
                ('country', models.ForeignKey(to='main.Country')),
            ],
            options={
                'verbose_name': 'Member',
                'verbose_name_plural': 'Members',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('content', ckeditor.fields.RichTextField()),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=b'uploads/news/')),
                ('date', models.DateTimeField()),
                ('is_active', models.BooleanField(default=True, verbose_name=b'Published')),
                ('slug', models.SlugField(unique=True, verbose_name=b'URL')),
            ],
            options={
                'verbose_name': 'Post',
                'verbose_name_plural': 'Posts',
            },
        ),
    ]
