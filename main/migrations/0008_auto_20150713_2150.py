# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20150713_2148'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='image',
            field=sorl.thumbnail.fields.ImageField(upload_to=b'uploads/members', blank=True),
        ),
    ]
