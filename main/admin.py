from django.contrib import admin
from main.models import *
from sorl.thumbnail.admin import AdminImageMixin
from django_admin_bootstrapped.admin.models import SortableInline


class MenuItemInlineAdmin(admin.TabularInline, SortableInline):
    model = MenuItem
    extra = 0


@admin.register(Menu)
class AdminMenuItem(admin.ModelAdmin):
    list_display = ('title',)
    list_per_page = 30
    inlines = (MenuItemInlineAdmin,)


@admin.register(Post)
class AdminPost(AdminImageMixin, admin.ModelAdmin):
    list_display = ('title', 'preview', 'is_active', 'date')
    search_fields = ('title',)
    list_filter = ('is_active', 'date')
    ordering = ('-date',)
    list_per_page = 20
    date_hierarchy = 'date'
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Event)
class AdminEvent(AdminImageMixin, admin.ModelAdmin):
    list_display = ('title', 'preview', 'is_active', 'date')
    search_fields = ('title',)
    list_filter = ('is_active', 'date')
    ordering = ('-date',)
    list_per_page = 20
    date_hierarchy = 'date'
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Document)
class AdminDocument(admin.ModelAdmin):
    list_display = ('title', 'is_active', 'date')
    search_fields = ('title',)
    list_filter = ('is_active', 'date')
    ordering = ('-date',)
    list_per_page = 20
    date_hierarchy = 'date'
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Country)
class AdminCategory(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)


@admin.register(Member)
class AdminMember(AdminImageMixin, admin.ModelAdmin):
    list_display = ('name', 'country', 'email')
    search_fields = ('name',)
    ordering = ('name',)
    list_filter = ('country', )
    list_per_page = 20

