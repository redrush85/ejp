from main.views import *
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    url(r'^$', 'main.views.home', name="home-page"),

    url(r'^news/(?P<slug>[-\w]+)/$', PostDetailView.as_view(), name='post-detail'),
    url(r'^news/$', PostListView.as_view(), name='post-list'),

    url(r'^events/(?P<slug>[-\w]+)/$', EventDetailView.as_view(), name='event-detail'),
    url(r'^events/$', EventListView.as_view(), name='event-list'),

    url(r'^documents/(?P<slug>[-\w]+)/$', DocumentDetailView.as_view(), name='document-detail'),
    url(r'^documents/$', DocumentListView.as_view(), name='document-list'),

    url(r'^members/(?P<pk>[0-9]+)/$', MemberDetailView.as_view(), name='member-detail'),
    url(r'^members/$', MemberListView.as_view(), name='member-list'),

    url(r'^feedback/$', FeedbackCreateView.as_view(), name='feedback'),
    url(r'^thanks/$', ThanksView.as_view(), name='thanks'),

    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^admin/', include(admin.site.urls)),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
